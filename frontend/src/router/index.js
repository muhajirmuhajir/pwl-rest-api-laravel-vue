import { createWebHistory, createRouter } from "vue-router";
import ListContact from "../components/ListContact.vue";
import DetailContact from "../components/DetailContact.vue";

const routes = [
  { path: "/", name: "Home", component: ListContact },
  { path: "/contacts/:id", name: "Detail", component: DetailContact },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
