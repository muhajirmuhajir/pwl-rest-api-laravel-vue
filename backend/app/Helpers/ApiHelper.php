<?php

namespace App\Helpers;

use Exception;

class ApiHelper{

    public static function success($data):array{
        return [
            'code'=> 200,
            'message'=> 'success',
            'data'=>$data
        ];
    }

    public static function error(Exception $error):array{
        return [
            'code'=> $error->getCode(),
            'message'=> $error->getMessage(),
            'data'=>null
        ];
    }
}
