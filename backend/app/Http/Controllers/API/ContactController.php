<?php

namespace App\Http\Controllers\API;

use App\Models\Contact;
use App\Helpers\ApiHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;

class ContactController extends Controller
{
    public function create(Request $request)
    {
        try {
            $fields = $request->validate([
                'name' => 'required|string',
                'email' => 'string',
                'phone' => 'string',
                'address' => 'string',
            ]);

            $contact = Contact::create($fields);
            return ApiHelper::success($contact);
        } catch (Exception $th) {
            return ApiHelper::error($th);
        }
    }

    public function read($id)
    {
        try {
            $contact = Contact::findOrFail($id);
            return ApiHelper::success($contact);
        } catch (Exception $th) {
            return ApiHelper::error($th);
        }
    }

    public function readAll()
    {
        $contacts = Contact::all(['id','name', 'phone']);

        return ApiHelper::success($contacts);
    }

    public function update(Request $request, $id)
    {
        try {
            $contact = Contact::findOrFail($id);
            $contact->update($request->all());
            return ApiHelper::success($contact);
        } catch (Exception $th) {
            return ApiHelper::error($th);
        }
    }

    public function delete($id)
    {
        try {

            $contact = Contact::destroy($id);
            return ApiHelper::success($contact);
        } catch (Exception $th) {
            return ApiHelper::error($th);
        }
    }
}
